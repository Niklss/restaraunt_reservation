FROM python:slim

ENV PYTHONUNBUFFERED=1

WORKDIR /opt
COPY requirements.txt /requirements.txt
RUN pip install --upgrade pip && pip install -r /requirements.txt

COPY . /opt
