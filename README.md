# How to run

### 1. Environment variables
1. Rename .env.example to .env
2. Replace telegram bot token with yours as in .env.example

### 2. Build image
docker build -t __image_name__ .

### 3. Run image with .env
docker run --name __container_name__ --env-file .env __image_name__ python main.py