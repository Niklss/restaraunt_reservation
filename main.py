from src import BotFactory

if __name__ == '__main__':
    tg_bot = BotFactory.create_tg_pooling_bot()
    tg_bot.start_polling()