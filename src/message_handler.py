from datetime import datetime
from abc import ABC

from .replies import TelegramBotReplies


class AbstractMessageHandler(ABC):
    def handle(self, message: str, user_id: int, *args, **kwargs) -> str:
        pass


class Restaurant(AbstractMessageHandler):
    def __init__(self):
        self.__message_reply_dict = {
            '/start': self.__start,
            'Зарезервировать': self.__reserve,
            'Чекнуть': self.__check_reservation
        }
        self.__next_message_handler = {}
        self.__reservations = {}

    def handle(self, message: str, user_id: int, *args, **kwargs):
        next_handler = self.__next_message_handler.get(user_id, None)
        if next_handler:
            return next_handler(message, user_id, *args, **kwargs)

        reply_funct = self.__message_reply_dict.get(message, None)
        if not self.__message_reply_dict.get(message, None):
            return None
        return reply_funct(message, user_id, *args, **kwargs)

    def __start(self, *args, **kwargs) -> str:
        return TelegramBotReplies.start

    def __reserve(self, message, user_id, *args, **kwargs) -> str:
        reservation = self.__reservations.get(user_id, None)
        if not reservation:
            self.__next_message_handler[user_id] = self.__receive_people_amount
            return TelegramBotReplies.ask_people_amount

        else:
            self.__next_message_handler[user_id] = None
            return TelegramBotReplies.already_reserved

    def __check_reservation(self, message, user_id, *args, **kwargs) -> str:
        reservation = self.__reservations.get(user_id, None)
        if not reservation:
            return TelegramBotReplies.no_reservation
        return TelegramBotReplies.check_reservation.format(amount=reservation.get('amount', None),
                                                           time=reservation.get('time', None))

    def __receive_people_amount(self, message, user_id, *args, **kwargs):
        try:
            int(message)
        except ValueError:
            self.__next_message_handler[user_id] = self.__receive_people_amount
            return TelegramBotReplies.input_right_int_value
        self.__update_reservation(user_id, "amount", message)
        self.__next_message_handler[user_id] = self.__receive_reservation_time
        return TelegramBotReplies.ask_reserve_time

    def __receive_reservation_time(self, message, user_id, *args, **kwargs):
        try:
            datetime.strptime(message, "%d.%m.%Y %H:%M")
        except ValueError:
            self.__next_message_handler[user_id] = self.__receive_reservation_time
            return TelegramBotReplies.input_right_date_value
        self.__update_reservation(user_id, "time", message)
        self.__next_message_handler[user_id] = None
        return TelegramBotReplies.reservation_finished

    def __update_reservation(self, user_id, key, value):
        if not self.__reservations.get(user_id, None):
            self.__reservations[user_id] = {}

        self.__reservations[user_id].update({key: value})
