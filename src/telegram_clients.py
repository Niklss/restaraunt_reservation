import os
import logging

from .abstract_clients import AbstractPoolingClient
from .message_handler import Restaurant

import telebot


class TelegramClient(AbstractPoolingClient):
    def __init__(self):
        logger = telebot.logger
        logger.setLevel(level=logging.DEBUG)
        self.bot = telebot.TeleBot(os.getenv("TELEGRAM_TOKEN", None), parse_mode='Markdown')
        self.bot.register_message_handler(self._handle_message)
        self.handler = Restaurant()

    def _handle_message(self, message: telebot.types.Message, *args, **kwargs) -> None:
        if not message.text:
            return None
        reply = self.handler.handle(message.text, message.chat.id)
        if reply:
            self._send_message(message, reply)
        return None

    def _send_message(self, income_message: telebot.types.Message, text, *args, **kwargs):
        self.bot.send_message(income_message.chat.id, text)

    def start_polling(self, *args, **kwargs):
        self.bot.polling()


