from .abstract_clients import AbstractPoolingClient
from .telegram_clients import TelegramClient


class BotFactory:
    @staticmethod
    def create_tg_pooling_bot() -> AbstractPoolingClient:
        return TelegramClient()
