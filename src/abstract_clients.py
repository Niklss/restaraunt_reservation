from abc import ABC, abstractmethod
from .message_handler import AbstractMessageHandler


class AbstractPoolingClient(ABC):
    handler: AbstractMessageHandler

    @abstractmethod
    def _handle_message(self, *args, **kwargs):
        pass

    @abstractmethod
    def start_polling(self, *args, **kwargs):
        pass
